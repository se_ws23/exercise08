package biz.ibdb

case class Metadata(bAuthor: String, bTitle: String, numPages: Int)