package biz.ibdb

object Search:
  def fromISBN(isbn: Array[Int]): Option[Metadata] = isbn match
    case  Array(0, 2, 6, 2, 1, 6, 2, 0, 9, 1) =>
      Some(Metadata("Benjamin C. Pierce", "Types and Programming Languages", 648))
    case _ => None
