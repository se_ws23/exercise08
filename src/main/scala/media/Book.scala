package media

import media.Slicer.sliceFormat

class Book(private var isbn: Array[Int], private var metadata: Option[biz.ibdb.Metadata]) extends Entity:
  def setISBN(id: Array[Int]): Unit =
    if (validateIsbnCheckDigit(id))
      isbn = id

  def formatIsbn: Either[ISBNFormatError, String] = isbn.length match
    case 10 => Right(s"${isbn(0)}-${sliceFormat(isbn,1,3)}-${sliceFormat(isbn,4,8)}-${isbn(9)}")
    case 13 => Right(s"${sliceFormat(isbn,0,2)}-${isbn(3)}" +
      s"-${sliceFormat(isbn,4,6)}-${sliceFormat(isbn,7,11)}-${isbn(12)}")
    case _ => Left(ISBNFormatError("invalid number of digits"))

  def fetchMetadata(): Unit = metadata = biz.ibdb.Search.fromISBN(isbn)
  def getAuthor: String = if metadata.isDefined then metadata.get.bAuthor else ""
  def getTitle: String = if metadata.isDefined then metadata.get.bTitle else ""
  def getPages: Int = if metadata.isDefined then metadata.get.numPages else 0

  private def validateIsbnCheckDigit(isbn: Array[Int]): Boolean = isbn.length match
    case 10 =>
      var r = 0
      for (i <- 0 until isbn.length - 1)
        r += (i + 1) * isbn(i)
      isbn(9) == r % 11
    case 13 =>
      var r = 0
      for (i <- 0 until isbn.length - 1)
        r = r + isbn(i) * scala.math.pow(3, i % 2).toInt
      isbn(12) == 10 - (r % 10)
    case _ => false