package media

object Slicer:
  def sliceFormat(arr: Array[Int], l: Int, r: Int): String =
    arr.slice(l, r+1).mkString